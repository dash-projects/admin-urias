<?php

namespace AdminUrias;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'titularName', 'accountNumber',
  ];
}
