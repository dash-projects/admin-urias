<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::auth();

Route::get('/inicio', 'HomeController@index');
Route::get('/registro', function () {
    return view('records');
});
Route::get('/consulta', function () {
    return view('search');
});
Route::get('/proveedores', function () {
    return view('providers');
});
Route::get('/cuenta/detallado', function () {
    return view('details');
});
